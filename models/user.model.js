const mongoose = require('mongoose');
const {isEmail} = require('validator'); // validateur d'email importé par la commande npm i -s validator
const bcrypt = require('bcrypt');

//Création du modèle de user
const userSchema = new mongoose.Schema(
    {
        pseudo: {
            type: String,
            minLength: 3,
            maxLength: 25,
            unique: true,
            trimp: true
        },
        email:{
            type: String,
            required: true,
            unique: true,
            validate: [isEmail],
            lowercase: true,
            trim: true,
        },
        password: {
            type: String,
            required: true,
            max: 1024,
            minLength: 8
        },
        picture :{
            type: String,
            default: "./uploads/profil/random-user.png"
        },
        bio: {
            type : String,
            max:1024,
        },
        followers:{
            type: [String] // Un tableau de String correspondant aux id des users
        },
        following:{
            type: [String] // Un tableau de String correspondant aux id des users
        },
        likes: {
            type:[String] // Un tableau de String correspondant aux id des posts pour ne pas liker un post déjà liké
        }

    },
    {
        timestamps: true, // Pour savoir quand est ce que le user est connecté , il faut le mettre à la fin 
    }
)
// Cryptage du mot de passe
//Play function before save into display: 'bloc', 
//il faut charger la bibliothèque de cryptage de node avec la commande npm i -s bcrypt
userSchema.pre('save', async function(next){
 const salt = await bcrypt.genSalt();
 this.password = await bcrypt.hash(this.password, salt);
 next();// pour passer à la suite
});

//Trouver l'utilisateur lors d'un login 
userSchema.statics.login = async function (email, password){
    const user = await this.findOne({email});
    if (user){
        const auth = await bcrypt.compare(password, user.password);
        if (auth){
            return user;
           }
           throw Error('Incorrect password');
    }
    throw Error('Incorrect email')
    
}
const UserModel = mongoose.model('user', userSchema); // C'est pour exporter le modèle user

module.exports = UserModel;