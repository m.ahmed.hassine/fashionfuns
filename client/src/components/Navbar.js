import React, { useContext } from "react";
import { NavLink } from "react-router-dom";
import { UidContext } from "./AppContext";
import { useSelector } from "react-redux";
import Logout from "./Log/Logout";

const Navbar = () => {
  const Uid = useContext(UidContext);
  const userData = useSelector((state) => state.userReducer);
  return (
    <nav>
      <div className="nav-container">
        <div className="logo">
          <NavLink exact to="/">
            <div className="logo">
              <img src="./img/icon.png" alt="icon" />
              <h3>Fashion Funs</h3>
            </div>
          </NavLink>
        </div>
        {Uid ? (
          <ul>
            <li></li>
            <li className="welcome">
              <NavLink exact to="/profil">
                <h5>Bienvenue {userData.pseudo}</h5>
              </NavLink>
            </li>
            <Logout />
          </ul>
        ) : (
          <ul>
            <li></li>
            <NavLink exact to="/profil">
              <img src="./img/icons/login.svg" alt="login"></img>
            </NavLink>
          </ul>
        )}
      </div>
    </nav>
  );
};

export default Navbar;
