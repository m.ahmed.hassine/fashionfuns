const express = require('express');
const bodyParser = require ('body-parser'); // bibliotheque à importer npm i -s body-parser 
    // il va traiter la data de point a à un point b
const cookieParser = require('cookie-parser');
const userRoutes = require('./routes/user.routes');
const postRoutes = require('./routes/post.routes');
require('dotenv').config({path: './config/.env'});
require('./config/db.js');
const {checkUser, requireAuth} = require('./middleware/auth.middleware');
const cors = require('cors') ; // pour autoriser l'accès à l'api crée
const app = express();

const corsOptions = {
    origin: process.env.CLIENT_URL,
    credentials: true,
    'allowedHeaders' : ['sessionId', 'content-Type'],
    'exposedHeaders' : ['sessionId'],
    'methods' : 'GET,HEAD,PUT,PATCH,POST,DELETE',
    'preflightContinue' : false
}

//app.use(cors()); // comme ça on autorise tout le monde
app.use(cors(corsOptions));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

app.use(cookieParser());

// jwt check user
app.get('*', checkUser);
app.get('/jwtid', requireAuth, (req, res) => {
    res.status(200).send(res.locals.user._id);
})

//routes toujours en avant dernier
app.use('/api/user', userRoutes);
app.use('/api/post', postRoutes);


//server: il faut qu'il soit toujours à la fin du fichier pour lire tout ce qui est avant
app.listen(process.env.PORT, () => {
    console.log(`Listening on port ${process.env.PORT}`);
})