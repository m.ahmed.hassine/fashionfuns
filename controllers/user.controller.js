const UserModel = require('../models/user.model');
const ObjectID = require('mongoose').Types.ObjectId;

//Obtenir tous les utilisateurs
module.exports.getAllUsers = async (req, res) => {
    const users = await UserModel.find().select('-password'); //Selectionner toutes les données sauf le password
    res.status(200).json(users);
}

//Obtenir un seul utilisateur
module.exports.getUserInfos = async (req, res) => {
    console.log(req.params);
    if(!ObjectID.isValid (req.params.id)) 
    return res.status(400).send('ID unknown : ' + req.params.id)
    
    UserModel.findById(req.params.id, (err, docs) => {
        if (!err) res.send(docs);
        else console.log('ID unknown'+ err)
    }).select('-password'); //sans le password
}

//update user
module.exports.updateUser = async (req, res) => {
    //Si le user n'existe pas on arrete
    if(!ObjectID.isValid (req.params.id)) 
    return res.status(400).send('ID unknown : ' + req.params.id)

    try {
        await UserModel.findByIdAndUpdate(
            {_id :req.params.id},
            {
                $set: 
                {
                    bio: req.body.bio
                }
            },
            {
                new: true, upsert: true, setDefaultsOnInsert: true},
                (err, docs) => {
                    if(!err) return res.send(docs);
                    if(err) return res.status(500).send({message : err});
                }
         
        )
    } catch (error) {
        return res.status(500).json({message : error});
    }
}

//Supprimer user
module.exports.deleteUser = async (req, res) =>{
//Si le user n'existe pas on arrete
if(!ObjectID.isValid (req.params.id)) 
return res.status(400).send('ID unknown : ' + req.params.id)

try {
    await UserModel.remove({ _id: req.params.id}).exec();
    res.status(200).json({ message: "Successfully deleted. "});

} catch (error){
    res.status(500).json({ message :error });
}
}

//gerer les invitations

//follow
module.exports.follow = async (req, res) =>{
 //Si le user n'existe pas on arrete
if(!ObjectID.isValid (req.params.id) || !ObjectID.isValid(req.body.idToFollow)) 
return res.status(400).send('ID unknown : ' + req.params.id)

try {
    //add to the followers list
    await UserModel.findByIdAndUpdate(
        req.params.id,
        {$addToSet: { following : req.body.idToFollow}},
        {new: true, upsert: true},
        (err, docs) => {
            if (!err) res.status(201).json(docs);
            else return res.status(400).json(err);
        }
    );
    // add to the followings list 
    await UserModel.findByIdAndUpdate(
        req.body.idToFollow,
        {$addToSet: { followers: req.params.id}},
        { new: true, upsert: true },
        (err, docs) => {
            //if (!err) res.status(201).json(docs);
            if (err) return res.status(400).json(err);
        }
    )
}
catch (error){
    res.status(500).json({ message :error });
}   
}

//unfollow
module.exports.unfollow = async (req, res) =>{
    //Si le user n'existe pas on arrete
   if(!ObjectID.isValid (req.params.id) || !ObjectID.isValid(req.body.idToUnfollow)) 
   return res.status(400).send('ID unknown : ' + req.params.id)
   
   try {
       //add to the followers list
       await UserModel.findByIdAndUpdate(
           req.params.id,
           {$pull: { following : req.body.idToUnfollow}},
           {new: true, upsert: true},
           (err, docs) => {
               if (!err) res.status(201).json(docs);
               else return res.status(400).json(err);
           }
       );
       // add to the followings list 
       await UserModel.findByIdAndUpdate(
           req.body.idToUnfollow,
           {$pull: { followers: req.params.id}},
           { new: true, upsert: true },
           (err, docs) => {
               //if (!err) res.status(201).json(docs);
               if (err) return res.status(400).json(err);
           }
       )
   }
   catch (error){
       res.status(500).json({ message :error });
   }   
   }